import request from "../utils/request";

class GponService {

    getAll() {
        return request.get(`/inventory/gpon/`)
    }

    getStoAndVendor(sto, vendor) {
        return request.get(`/inventory/gpon/sto/${sto}/vendor/${vendor}`)
    }

    getId(id) {
        return request.get(`/inventory/gpon/${id}`)
    }

    getIpAdd(ip) {
        return request.get(`/inventory/gpon/ip-gpon/${ip}`)
    }

    getHostname(hostname) {
        return request.get(`/inventory/gpon/hostname/${hostname}`)
    }

    getVendor(vendor) {
        return request.get(`/inventory/gpon/vendor/${vendor}`)
    }

    doDownload(filename) {
        return request.get(`inventory/gpon/download/template/${filename}`, {responseType: 'blob'})
    }

    store(data) {
        return request.post(`/inventory/gpon`, data)
    }

    storeBatch(data) {
        return request.post(`/inventory/gpon/batch`, data)
    }

    update(id, data) {
        return request.put(`/inventory/gpon/${id}`, data)
    }

    delete(id) {
        return request.delete(`/inventory/gpon/${id}`)
    }

    getTypeOnt(vendor) {
        return request.get(`/inventory/gpon/ont-type/vendor/${vendor}`)
    }

    sortOrder(prop) {
        return function(a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        }
    }
}

export default new GponService();