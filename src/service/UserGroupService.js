import request from "../utils/request";

class UserGroupService {

    getByUsernam(username) {
        return request.get(`/uaa/group/username/${username}`)
    }

    store(data) {
        return request.post(`/uaa/group`, data)
    }

    deleteByUsernam(username) {
        return request.delete(`/uaa/group/username/${username}`)
    }

    sortOrder(prop) {
        return function(a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        }
    }
}

export default new UserGroupService()