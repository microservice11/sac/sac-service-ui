import request from "../utils/request";

class LogConfig {

    getAll() {
        return request.get(`/log/config`)
    }

    getByIdAndUsername(id, username) {
        return request.get(`/log/config/${id}/username/${username}`)
    }

    getByUsername(username) {
        return request.get(`/log/config/username/${username}`)
    }

    getLimit(limit) {
        return request.get(`/log/config/limit/${limit}`)
    }

    sortOrder(prop) {
        return function(a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        }
    }
}

export default new LogConfig