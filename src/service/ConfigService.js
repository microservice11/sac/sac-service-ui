import request from "../utils/request";

class ConfigService {

    doDownload(filename) {
        return request.get(`config/download/template/${filename}`, {responseType: 'blob'})
    }

    checkUnreg(gpons, vendor) {
        let data = []
        for (let i in gpons) {
            let gpon = {
                ip_gpon: gpons[i].id,
                slot_port: gpons[i].slot + '-' + gpons[i].port,
                onu_id: gpons[i].onu
            }
            data.push(gpon)
        }
        return request.post(`/inventory/config/check/unreg/vendor/${vendor}`, data)
    }

    checkReg(gpons, vendor) {
        let data = []
        for (let i in gpons) {
            let gpon = {
                ip_gpon: gpons[i].id,
                slot_port: gpons[i].slot + '-' + gpons[i].port,
                onu_id: gpons[i].onu
            }
            data.push(gpon)
        }
        return request.post(`/inventory/config/check/reg/vendor/${vendor}`, data)
    }

    checkService(gpons, vendor) {
        let data = []
        for (let i in gpons) {
            let gpon = {
                ip_gpon: gpons[i].id,
                slot_port: gpons[i].slot + '-' + gpons[i].port,
                onu_id: gpons[i].onu
            }
            data.push(gpon)
        }
        return request.post(`/inventory/config/check/service/vendor/${vendor}`, data)
    }

    batch(gpons) {
        return request.post(`/inventory/config/batch`, gpons)
    }

    create(gpons) {
        return request.post(`/inventory/config/create`, gpons)
    }
}

export default new ConfigService();