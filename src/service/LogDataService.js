import request from "../utils/request";

class LogConfig {

    getAll() {
        return request.get(`/log/data`)
    }

    getById(id) {
        return request.get(`/log/data/${id}`)
    }
}

export default new LogConfig