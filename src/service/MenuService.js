import request from "../utils/request";

class MenuService {
    getAll() {
        return request.get('/uaa/menu')
    }

    getByIdAndType(id, type) {
        return request.get(`/uaa/menu/id/${id}/type/${type}`)
    }

    getByParent() {
        return request.get(`/uaa/menu/parent`)
    }

    getByParentId(id) {
        return request.get(`/uaa/menu/parent/${id}`)
    }

    getClientId() {
        return request.get(`/uaa/menu/client`)
    }

    store(data) {
        return request.post('/uaa/menu', data)
    }

    update(data) {
        return request.put(`/uaa/menu`, data)
    }

    delete(id, type) {
        return request.delete(`/uaa/menu/id/${id}/type/${type}`)
    }
}

export default new MenuService()