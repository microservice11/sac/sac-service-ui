import request from "../utils/request";

class FormApp {
    getAll() {
        return request.get('/uaa/form')
    }

    getByIdAndType(id, type) {
        return request.get(`/uaa/form/id/${id}/type/${type}`)
    }

    store(data) {
        return request.post('/uaa/form', data)
    }

    update(data) {
        return request.put('/uaa/form', data)
    }

    delete(id, type) {
        return request.delete(`/uaa/form/id/${id}/type/${type}`)
    }

}

export default new FormApp()