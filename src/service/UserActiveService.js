import request from "../utils/request";

class UserActiveService {

    getAll() {
        return request.get(`/uaa/auth/active`)
    }

    delete(id) {
        return request.delete(`/uaa/auth/terminate/${id}`)
    }

    sortOrder(prop) {
        return function(a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        }
    }
}

export default new UserActiveService();