import request from "../utils/request";

class StoService {

    getAll() {
        return request.get(`/inventory/sto`)
    }

    getId(id) {
        return request.get(`/inventory/sto/${id}`)
    }

    getAlias(alias) {
        return request.get(`/inventory/sto/alias/${alias}`)
    }

    getWitel(witel) {
        return request.get(`/inventory/sto/witel/${witel}`)
    }

    getWitelAndVendor(witel, vendor) {
        return request.get(`/inventory/sto/witel/${witel}/vendor/${vendor}`)
    }

    store(data) {
        return request.post(`/inventory/sto`, data)
    }

    update(id, data) {
        return request.put(`/inventory/sto/${id}`, data)
    }

    delete(id) {
        return request.delete(`/inventory/sto/${id}`)
    }

    sortOrder(prop) {
        return function(a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        }
    }
}

export default new StoService();