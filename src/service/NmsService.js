import request from "../utils/request";

class NmsService {

    getAll() {
        return request.get(`/inventory/nms`)
    }

    getId(id) {
        return request.get(`/inventory/nms/${id}`)
    }

    getIpServer(ipServer) {
        return request.get(`/inventory/nms/ip-server/${ipServer}`)
    }

    getNama(nama) {
        return request.get(`/inventory/nms/nama/${nama}`)
    }

    getVendor() {
        return request.get(`/inventory/nms/vendor`)
    }

    getProtocol() {
        return request.get(`/inventory/nms/protocol`)
    }

    store(data) {
        return request.post(`/inventory/nms`, data)
    }

    update(id, data) {
        return request.put(`/inventory/nms/${id}`, data)
    }

    delete(id) {
        return request.delete(`/inventory/nms/${id}`)
    }

    sortOrder(prop) {
        return function(a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        }
    }
}

export default new NmsService();