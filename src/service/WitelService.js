import request from "../utils/request";

class WitelService {

    getAll() {
        return request.get(`/inventory/witel`)
    }

    getId(id) {
        return request.get(`/inventory/witel/${id}`)
    }

    getRegional(regional) {
        return request.get(`/inventory/witel/regional/${regional}`)
    }

    getRegionals(regionals) {
        return request.get(`/inventory/witel/regionals/${regionals}`)
    }

    getRegionalAndVendor(regional, vendor) {
        return request.get(`/inventory/witel/regional/${regional}/vendor/${vendor}`)
    }

    store(data) {
        return request.post(`/inventory/witel`, data)
    }

    update(id, data) {
        return request.put(`/inventory/witel/${id}`, data)
    }

    delete(id) {
        return request.delete(`/inventory/witel/${id}`)
    }

    sortOrder(prop) {
        return function(a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        }
    }
}

export default new WitelService();