import request from "../utils/request";

class RoleService {

    getAll() {
        return request.get('/uaa/role')
    }

    getById(id) {
        return request.get(`/uaa/role/${id}`)
    }

    store(data) {
        return request.post(`/uaa/role`, data)
    }

    update(data) {
        return request.put(`/uaa/role`, data)
    }

    delete(id) {
        return request.delete(`/uaa/role/${id}`)
    }
}

export default new RoleService()