import request from "../utils/request";

class RegionalService {

    getAll() {
        return request.get(`/inventory/regional`)
    }

    getVendor(vendor) {
        return request.get(`/inventory/regional/vendor/${vendor}`)
    }

    getVendorCreate(vendor) {
        return request.get(`/inventory/regional/vendor/${vendor}/param/1`)
    }

    get(id) {
        return request.get(`/inventory/regional/${id}`)
    }

    store(data) {
        return request.post(`/inventory/regional`, data)
    }

    update(id, data) {
        return request.put(`/inventory/regional/${id}`, data)
    }

    delete(id) {
        return request.delete(`/inventory/regional/${id}`)
    }

    sortOrder(prop) {
        return function(a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        }
    }
}

export default new RegionalService();