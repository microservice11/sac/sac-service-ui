import request from "../utils/request";

class UserService {

    getAll() {
        return request.get(`/uaa/user`)
    }

    getByUsername(username) {
        return request.get(`/uaa/user/${username}`)
    }

    store(data) {
        return request.post(`/uaa/user`, data)
    }

    update(data) {
        return request.put(`/uaa/user`, data)
    }

    delete(username) {
        return request.delete(`/uaa/user/${username}`)
    }

    resetPassword(data, param) {
        if (param === 0) {
            return request.post(`/uaa/user/password/reset/${data.username}`, data)
        } else {
            return request.post(`/uaa/user/password/change/${data.username}`, data)
        }
    }

    sortOrder(prop) {
        return function(a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        }
    }
}

export default new UserService();