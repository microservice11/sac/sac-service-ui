import requestAuth from "../utils/request";
import {parseJwt} from "../utils/util";

export default{
    state: {
        sub: null,
        aud: null,
        nbf: null,
        scope: [],
        default_user: false,
        iss: null,
        exp: null,
        iat: null,
        header_token: null,
        verify_token: null,
        refresh_token: null,
        username: null,
        fullname: null,
        isLoggedIn: false,
        isDefaultUser: false
    },
    mutations: {
        SET_TOKEN(state, data) {
            state.sub = data.sub
            state.aud = data.aud
            state.nbf = data.nbf
            state.scope = data.scope
            state.default_user = data.default_user
            state.iss = data.iss
            state.exp = data.exp
            state.iat = data.iat
            state.header_token = data.header_token
            state.verify_token = data.verify_token
            state.refresh_token = data.refresh_token
            state.fullname = data.full_name
            state.username = data.sub
            state.isLoggedIn = true
            state.isDefaultUser = data.default_user
        },
        RESET_USER(state) {
            state.sub = null
            state.aud = null
            state.nbf = null
            state.scope = []
            state.default_user = false
            state.iss = null
            state.exp = null
            state.iat = null
            state.header_token = null
            state.verify_token = null
            state.refresh_token = null
            state.username = null
            state.fullname = null
            state.isLoggedIn = false
            state.isDefaultUser = false
        },
    },
    getters:{
        getSub(state) {
            return state.sub
        },
        getAud(state) {
            return state.aud
        },
        getNbf(state) {
            return state.nbf
        },
        getScope(state) {
            return state.scope
        },
        getDefaultUser(state) {
            return state.default_user
        },
        getIss(state) {
            return state.iss
        },
        getExp(state) {
            return state.exp
        },
        getIat(state) {
            return state.iat
        },
        getHeaderToken(state) {
            return state.header_token
        },
        getVerifyToken(state) {
            return state.verify_token
        },
        getRefreshToken(state) {
            return state.refresh_token
        },
        getUsername(state) {
            return state.username
        },
        getFullname(state) {
            return state.fullname
        },
        isLoggedIn(state) {
            return state.isLoggedIn
        },
        isDefaultUser(state) {
            return state.isDefaultUser
        }
    },
    actions: {
        doLogin({commit}, {username, password, otp}) {
            return new Promise((resolve, reject) => {
                let data_login = { username, password, otp }
                requestAuth({
                    url: '/uaa/auth/login',
                    data: data_login,
                    method: 'post',
                }).then(async resp => {
                    commit('RESET_USER_TMP')
                    let data = parseJwt(resp.data)
                    data.full_name = resp.data.data.full_name
                    data.menus = resp.data.data.menus
                    commit('SET_TOKEN', data)
                    commit('SET_RESOURCE', data)
                    resolve(resp)
                }).catch(err => {
                    commit('RESET_USER')
                    commit('RESET_RESOURCE')
                    console.log(err)
                    reject(err)
                })

            })
        },
        doLoginForce({commit, dispatch}, {username, password, otp}) {
            return new Promise((resolve, reject) => {
                let data_login = { username, password, otp }
                requestAuth({
                    url: '/uaa/auth/logout/force',
                    data: data_login,
                    method: 'post',
                }).then(() => {
                    dispatch('doLogin', {username, password, otp}).then(resp => {
                        resolve(resp)
                    })
                }).catch(err => {
                    commit('RESET_USER')
                    commit('RESET_RESOURCE')
                    console.log(err)
                    reject(err)
                })
            })
        },
        refreshToken({commit, getters}) {
            return new Promise((resolve, reject) => {
                let token = getters.getRefreshToken
                let data_login = {
                    grant_type: 'refresh_token',
                    refresh_token: token
                }
                requestAuth({
                    url: '/uaa/auth/refresh',
                    data: data_login,
                    method: 'post',
                }).then(async resp => {
                    let data = parseJwt(resp.data)
                    data.full_name = resp.data.data.full_name
                    data.menus = resp.data.data.menus
                    commit('SET_TOKEN', data)
                    commit('SET_RESOURCE', data)
                    resolve(resp)
                }).catch(err => {
                    console.log('ERROR REFRESH DARI AUTH')
                    let resp = err.response
                    const {status, data} = resp
                    if (status === 400 && data.error === 'invalid_grant' &&
                        (data.error_description.includes('Invalid refresh token (expired): ') || data.error_description.includes('Invalid refresh token: '))) {
                        commit('RESET_USER')
                        commit('RESET_RESOURCE')
                        window.location = '/login'
                    }
                    reject(err)
                })
            })
        },
        logout({commit}) {
            return new Promise((resolve, reject) => {
                requestAuth({
                    url: '/uaa/auth/logout',
                    method:'post',
                }).then(resp => {
                    commit('RESET_USER')
                    commit('RESET_RESOURCE')
                    window.location = '/login'
                    resolve(resp)
                }).catch(err => {
                    commit('RESET_USER')
                    commit('RESET_RESOURCE')
                    window.location = '/login'
                    reject(err)
                })
            })
        }
    },
}