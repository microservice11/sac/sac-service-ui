export default {
    state: {
        resource_access: null,
        groups: [],
        menus: [],
        roles: [],
    },
    mutations: {
        SET_RESOURCE(state, data) {
            state.resource_access = data.resource_access
            state.groups = data.groups
            state.menus = data.menus
            state.roles = data.roles
        },
        RESET_RESOURCE(state) {
            state.resource_access = null
            state.groups = []
            state.menus = []
            state.roles = []
        }
    },
    getters: {
        getResource(state) {
            return state.resource_access
        },getGroups(state) {
            return state.groups
        },
        getMenus(state) {
            return state.menus
        },
        getRoles(state) {
            return state.roles
        },
    }
}