import requestAuth from "../utils/request";
import router from "../router";

export default {
    state: {
        usernameTmp: null,
        passwordTmp: null,
        is_valid: false
    },
    mutations: {
        SET_USER_TMP(state, {username, password}) {
            state.usernameTmp = username
            state.passwordTmp = password
            state.is_valid = true
        },
        RESET_USER_TMP(state) {
            state.usernameTmp = null
            state.passwordTmp = null
            state.is_valid = false
        },
    },
    getters: {
        getUsernameTmp(state) {
            return state.usernameTmp
        },
        getPasswordTmp(state) {
            return state.passwordTmp
        },
        isValidTmp(state) {
            return state.is_valid
        }
    },
    actions: {
        doChoiceOtp({commit}, {username, password}) {
            return new Promise((resolve, reject) => {
                let data_login = {
                    username: username,
                    password: password,
                }
                requestAuth({
                    url: '/uaa/auth/check',
                    data: data_login,
                    method: 'post',
                }).then(resp => {
                    let res = resp.data
                    if (res.status === 'SUCCESS') {
                        commit('SET_USER_TMP', {username, password})
                    } else {
                        commit('RESET_USER_TMP')
                    }
                    resolve(resp)
                }).catch(err => {
                    commit('RESET_USER_TMP')
                    reject(err)
                })
            })
        },
        doLogoutForce({commit}, {username, password, token}) {
            return new Promise((resolve, reject) => {
                let data_login = { username, password }
                requestAuth({
                    url: `/uaa/auth/logout/force/${token}`,
                    data: data_login,
                    method: 'post',
                }).then(async () => {
                    commit('SET_USER_TMP', {username, password})
                    await router.push('/choice/otp')
                    return Promise.resolve()
                }).catch(err => {
                    commit('RESET_USER')
                    console.log(err)
                    reject(err)
                })
            })
        },
        doLogoutForceCheck({commit, dispatch}, {username, password, token, choice}) {
            return new Promise((resolve, reject) => {
                let data_login = { username, password }
                requestAuth({
                    url: `/uaa/auth/logout/force/${token}`,
                    data: data_login,
                    method: 'post',
                }).then(async () => {
                    commit('SET_USER_TMP', {username, password})
                    await dispatch('redirectToCheckOTP', {username, password, choice})
                    resolve()
                }).catch(err => {
                    commit('RESET_USER')
                    console.log(err)
                    reject(err)
                })
            })
        },
        redirectToCheckOTP({commit}, {username, password, choice}) {
            return new Promise((resolve, reject) => {
                let data_login = {username, password}
                requestAuth({
                    url: `/uaa/auth/check/${choice}`,
                    data: data_login,
                    method: 'post',
                }).then(async () => {
                    commit('SET_USER_TMP', {username, password})
                    await router.push('/check/otp')
                    resolve()
                }).catch(err => {
                    commit('RESET_USER')
                    console.log(err.response)
                    reject(err)
                })
            })
        }
    }
}