import Vue from 'vue'
import Vuex from 'vuex'

import auth from "../store/auth";
import resource from "./resource";
import tmp from "../store/tmp";

import createPersistedState from "vuex-persistedstate"
import Cookies from 'js-cookie';

Vue.use(Vuex)

const tokenState = new createPersistedState({
    key: 'auth',
    paths: ['auth'],
    storage: {
        getItem: key => Cookies.get(key),
        setItem: (key, value) => Cookies.set(key, value),
        removeItem: key => Cookies.remove(key)
    }
})

const menuRoleState = new createPersistedState({
    key: 'resource',
    paths: ['resource'],
    storage: {
        getItem: key => window.localStorage.getItem(key),
        setItem: (key, value) => window.localStorage.setItem(key, value),
        removeItem: key => window.localStorage.removeItem(key)
    }
})

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        auth, tmp, resource,
    },
    plugins: [tokenState, menuRoleState],
})
