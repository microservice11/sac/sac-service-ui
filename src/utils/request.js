import axios from "axios";
import createAuthRefreshInterceptor from 'axios-auth-refresh'
import store from '../store'
import {parseJwt, getAccessToken} from "./util";

const service = axios.create({
    baseURL: process.env.VUE_APP_BE_URL,
})

service.interceptors.request.use(req => {
    if (req.url === '/uaa/auth/login' || req.url.includes('/uaa/auth/logout/force') || req.url.includes('/uaa/auth/check') || req.url.includes('/uaa/auth/refresh')) {
        req.headers['Content-Type'] = 'application/json'
    } else if (req.url === '/oauth2/token' || req.url === '/oauth2/introspect') {
        req.headers['Content-Type'] = 'application/x-www-form-urlencoded'
        req.headers['Authorization'] = `Basic d2ViOndlYi1wYXNzd29yZA==`
    } else {
        let access_token = getAccessToken()
        req.headers['Content-Type'] = 'application/json'
        req.headers['Authorization'] = `Bearer ${access_token}`
    }
    return req
}, error => {
    console.log('error interceptors request in request.js')
    return Promise.reject(error)
})

const refreshAuthLogic = async failedRequest => await service.post('/uaa/auth/refresh',
    {grant_type: 'refresh_token', refresh_token: store.getters.getRefreshToken})
    .then(async tokenRefreshResponse => {
        if (failedRequest.response.status === 401 && !store.getters.isLoggedIn) {
                return Promise.reject(failedRequest)
        }

        let data = parseJwt(tokenRefreshResponse.data)
        store.commit('SET_TOKEN', data)
        let access_token = getAccessToken()
        failedRequest.response.config.headers['Content-Type'] ='application/json'
        failedRequest.response.config.headers['Authorization'] = `Bearer ${access_token}`
        return Promise.resolve(failedRequest)
}, err => {
        let {status, data} = err.response
        let {error} = data
        if (status === 400 && error.code === 'invalid_grant') {
            store.commit('RESET_USER')
            window.location = '/login'
        }
        return Promise.reject(err)
})

createAuthRefreshInterceptor(service, refreshAuthLogic, {statusCodes: [401]});




export default service