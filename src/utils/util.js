import XLSX from "xlsx";
import store from '../store'

const convertExcelTojson = (data, type) => {
    return new Promise(resolve => {
        let files = data
        const reader = new FileReader()
        reader.readAsBinaryString(files)
        reader.onload = (e) => {
            //parser data
            const bstr = e.target.result
            const wb = XLSX.read(bstr, {type: 'binary'})

            //get first worksheet
            const wsname = wb.SheetNames[0]
            const ws = wb.Sheets[wsname]

            //convert to array object
            const datas = XLSX.utils.sheet_to_json(ws, {header: type})

            //loop array object to dynamic form
            let configs = []
            for (let data of datas) {
                configs.push(data)
            }
            console.log(configs)
            resolve(configs)
        }
    })
}

const loadMenu = (menu = null) => {
    let data = null
    if (menu !== null) {
        if (menu.form_app != null) {
            let fullPath = menu.path
            if (menu.path_parameter != null && menu.path_parameter !== '') {
                fullPath += `/:${menu.path_parameter}`
            }
            data = {
                path: fullPath,
                name: menu.form_app.form,
                component: () => import(`../views/${menu.form_app.form}.vue`)
            }
        }
    }
    return Promise.resolve(data)
}

const loadMenuSync = function() {
    let routes = []
    const menus = store.getters.getMenus
    for (let menu of menus) {
        if (menu.form_app != null) {
            let fullPath = menu.path
            if (menu.path_parameter != null && menu.path_parameter !== '') {
                fullPath += `/:${menu.path_parameter}`
            }
            let data = {
                path: fullPath,
                name: menu.form_app.form,
                component: () => import(`../views/${menu.form_app.form}.vue`)
            }
            routes.push(data)
        }
    }
    return routes
}

const loadEachRouter = (to, from, next) => {
    const isLoggedIn = store.getters.isLoggedIn
    const isValid = store.getters.isValidTmp
    if (to.name !== 'LoginPage') {
        if (to.name === 'CheckOtp') {
            if (isValid) {
                next()
            } else {
                next({
                    name: 'LoginPage',
                    query: {
                        redirect: to.path
                    }
                })
            }
        } else if (to.name === 'ChoiceOtp') {
            if (isValid) {
                next()
            } else {
                next({
                    name: 'LoginPage',
                    query: {
                        redirect: to.path
                    }
                })
            }
        } else if (to.name === 'Home') {
            if (isValid) {
                next({
                    name: 'Dashboard',
                    query: {
                        redirect: to.path
                    }
                })
            } else {
                next({
                    name: 'LoginPage',
                    query: {
                        redirect: to.path
                    }
                })
            }
        } else if (to.name === 'Dashboard') {
            if (isLoggedIn) {
                next()
            } else {
                next({
                    name: 'LoginPage',
                    query: {
                        redirect: to.path
                    }
                })
            }
        }else if (isLoggedIn) {
            next()
        } else {
            next({
                name: 'LoginPage',
                query: {
                    redirect: to.path
                }
            })
        }
    } else {
        next()
    }
    return Promise.resolve()
}

const parseJwt = (response) => {
    let token = response.data.access_token
    let header_token = token.split('.')[0]
    let verify_token = token.split('.')[2]
    let base64Url = token.split('.')[1]
    let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
    let jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(''))

    let data = JSON.parse(jsonPayload)
    data.header_token = header_token
    data.verify_token = verify_token
    data.refresh_token = response.data.refresh_token

    data.roles = []
    let {resource_access} = data
    for (let key in resource_access) {
        for (let role of resource_access[key].roles) {
            data.roles.push(`${role}_${key.toUpperCase()}`)
        }
    }

    return data;
}

const getAccessToken = () => {
    let body = {
        sub: store.getters.getSub,
        aud: store.getters.getAud,
        resource_access: store.getters.getResource,
        nbf: store.getters.getNbf,
        scope: store.getters.getScope,
        default_user: store.getters.getDefaultUser,
        iss: store.getters.getIss,
        groups: store.getters.getGroups,
        exp: store.getters.getExp,
        iat: store.getters.getIat,
    }

    let header_token = store.getters.getHeaderToken
    // let body_token = Buffer.from(JSON.stringify(body)).toString("base64");
    // let body_token = encodeURIComponent(btoa(JSON.stringify(body)).split('').map(function(c) {
    //     return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    // }).join(''))
    let body_token = btoa(JSON.stringify(body)
        .replace("http://", "http:\\/\\/")
        .replace("https://", "https:\\/\\/")
    ).replace(/=/g, "")
    let verify_token = store.getters.getVerifyToken

    return `${header_token}.${body_token}.${verify_token}`
}

const checkError400 = (ex, dlgMsg, error_payload) => {
    let response = ex.response
    let {status, data} = response
    if (status === 400 || status === 404) {
        let {error} = data
        if (error && status === 400 && error_payload != null) {
            let {error_details} = error
            if (error_details) {
                for (let error_detail of error_details) {
                    if (error_detail.locationType === 'Field') {
                        for (let key of Object.keys(error_payload)) {
                            if (error_detail.location === key) {
                                error_payload[key] = [error_detail.message]
                            }
                        }
                    }
                }
            }
            dlgMsg.doOpenMsg(error.desc)
        } else {
            dlgMsg.doOpenMsg(data.message)
        }
    } else {
        if (data) {
            if (data.message) {
                dlgMsg.doOpenMsg(data.message)
            } else {
                dlgMsg.doOpenMsg('ERROR EXCEPTION')
            }
        } else {
            dlgMsg.doOpenMsg('ERROR EXCEPTION')
        }
    }
    return response;
}

const checkError404 = (ex, dlgMsg) => {
    checkError400(ex, dlgMsg, null)
}

export { convertExcelTojson, loadMenu, loadMenuSync, loadEachRouter, parseJwt, getAccessToken, checkError400, checkError404 }