import Vue from 'vue'
import VueRouter from 'vue-router'
import {loadEachRouter, loadMenuSync} from "../utils/util";

Vue.use(VueRouter)

let routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/dashboard'
  },
  {
    path: '/login',
    name: 'LoginPage',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/check/otp',
    name: 'CheckOtp',
    component: () => import('../views/CheckOtp.vue')
  },
  {
    path: '/choice/otp',
    name: 'ChoiceOtp',
    component: () => import('../views/ChoiceOtp.vue')
  },
  {
    path: '/progress/batch',
    name: 'ProgressBatch',
    component: () => import('../views/ProgressBatch.vue')
  },
  {
    path: '/progress/batch/:proccess_id',
    name: 'ProgressBatchDetail',
    component: () => import('../views/ProgressBatchDetail.vue')
  },
  // {
  //   path: '/data/regional',
  //   name: 'Regional',
  //   component: () => import('../views/Regional.vue')
  // }
]

for (const route of loadMenuSync()) {
  routes.push(route)
}

let router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(async (to, from, next) => {
  await loadEachRouter(to, from, next)
})

export default router
